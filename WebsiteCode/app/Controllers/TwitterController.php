<?php

namespace itechTest\App\Controllers;

use itechTest\Components\Social\Twitter\TwitterManager;


/**
 * Class TwitterController
 *
 * @package itechTest\App\Controllers
 */
class TwitterController extends BaseController
{
    /**
     * This will reload the timeline by refreshing
     *
     * @throws \Exception
     */
    public function getTwitter(): void
    {
        $minimumRefreshTime = 1;
        $refreshMinutes = (int)array_get_item($_GET, 'refresh', $minimumRefreshTime);
        $refreshMinutes = max($minimumRefreshTime, $refreshMinutes);
        $listOfCounts = range(20, TwitterManager::MAX_ALLOWABLE_COUNT, 20);
        $listOfRefreshMins = range($minimumRefreshTime, 30, 5);


        // fetch the tweets
        $tweets = $this->fetchTweets();
        [
            'twitterHandle'    => $twitterHandle,
            'count'            => $count,
            'response'         => $response,
            'errorMessage'     => $errorMessage,
            'errorHasOccurred' => $errorHasOccurred,
            //'statusCode'       => $statusCode,
        ] = $tweets;


        // decode the json response in the items
        $items = json_decode($response, true);

        $data = compact('items', 'twitterHandle',
            'listOfCounts', 'refreshMinutes', 'errorHasOccurred',
            'errorMessage', 'listOfRefreshMins', 'count');
        \view()
            ->addHeadCss(asset('/css/home/iframe-widget.css'))
            ->render('home.twitter.timeline', $data);
    }


    /**
     * @return array
     */
    private function fetchTweets(): array
    {
        /** @var TwitterManager $twitterManager */
        $twitterManager = $this->getApplication()['twitter'];
        $count = (int)array_get_item($_GET, 'count');
        $twitterHandle = array_get_item($_GET, 'handle');

        // keep bbc as a fallback
        $twitterHandle = empty($twitterHandle) ? 'bbc' : $twitterHandle;


        $errorHasOccurred = false;
        $errorMessage = '';
        $response = '';
        if (null !== $twitterHandle) {
            // set the details for fetching the data here
            $twitterManager = $twitterManager->setTwitterHandle($twitterHandle)->setCount($count);
            $response = $twitterManager->initiateRequest();


            /*
             * Error Handling Mechanism here
             */
            if ($twitterManager->hasResponseException()) {
                $errorHasOccurred = true;
                $responseException = $twitterManager->getResponseException();
                $errorMessage = $responseException ? $responseException->getMessage() : 'Error Has Occurred';
            }
        }

        $count = $twitterManager->getCount(); // refresh the count
        $statusCode = $twitterManager->getResponseStatusCode();

        return compact('count', 'twitterHandle', 'response', 'errorMessage', 'errorHasOccurred', 'statusCode');

    }

    /**
     * @throws \Exception
     */
    public function getApiTweets(): void
    {
        // fetch the tweets
        $tweets = $this->fetchTweets();
        [
            'response'         => $response,
            'errorMessage'     => $errorMessage,
            'errorHasOccurred' => $errorHasOccurred,
            'statusCode'       => $statusCode,
        ] = $tweets;

        /*
         * Error Handling Mechanism For API
         */
        if ($errorHasOccurred) {
            $error = [
                'message'     => $errorMessage,
                'status_code' => $statusCode,
            ];

            $response = json_encode($error);
        }

        header('Content-Type: application/json');
        http_response_code($statusCode);

        echo $response;
    }

    /**
     * @throws \Exception
     */
    public function getIframe(): void
    {
        $count = (int)array_get_item($_GET, 'count');
        $twitterHandle = array_get_item($_GET, 'handle', 'bbc');
        $data = compact('count', 'twitterHandle');

        \view()->setThemeLayout('blank')->render('iframe.index', $data);
    }


     /**
     * @throws \Exception
     */
    public function saveTweets(): void
    {
        
        $avatar = array_get_item($_GET, 'avatar');
        $text = array_get_item($_GET, 'text');
        $name = array_get_item($_GET, 'name');
        $location = array_get_item($_GET, 'location');
        $description = array_get_item($_GET, 'description');
        $screenName = array_get_item($_GET, 'screenName');
        $retweets = array_get_item($_GET, 'retweets');
        $favourites = array_get_item($_GET, 'favourites');


        // Create connection
        //$conn = mysqli_connect("127.0.0.1", "root", "toor", "legendcorpdb");

        $servername = "localhost";
        $database = "legendcorpdb";
        $username = "root";
        $password = "toor";
        // Create connection

        //$mysqli = new \MySQLi($servername, $username, $password, $database) or die(mysqli_error());
        
        $conn = mysqli_connect($servername, $username, $password, $database);
        // Check connection
        if (!$conn) {
            //die("Connection failed: " . mysqli_connect_error());
            $msg = "Connection failed";
        }
        $msg = "Connected successfully";
        mysqli_close($conn);
        
        
        // Check connection
        $query="INSERT INTO tweets (avatar, text, name, location, description, screenName, retweets, favourites ) VALUES( '$avatar', '$text', '$name', '$location', '$description', '$screenName', '$retweets', '$favourites' )";

        $result = mysqli_query($conn, $query);
        //$fila = mysqli_fetch_assoc($result);

            $response =
            [
                'data' => [
                    'avatar' => $avatar,
                    'text' => $text,
                    'name' => $name,
                    'location' => $location,
                    'description' => $description,
                    'screenName' => $screenName,
                    'retweets' => $retweets,
                    'favourites' => $favourites
                ],
                'status' => 'OK',
                'msg' => 'test'
            ];
        

        $conn->close();
        /*
        * Construct Data Structure
        */
      

        /*
        * Format Data
        */
        $jsonResponse = json_encode ( $response, JSON_PRETTY_PRINT );

        /*
        * Prepare Response
        */
        header('content-type: application/json; charset=UTF-8');

        /*
        * ONLY if you want/need any domain to be able to access it.
        */
        header('Access-Control-Allow-Origin: *');

        /*
        * Send Response
        */
        print_r ( $jsonResponse );
//        return json_encode($data);

        //\view()->setThemeLayout('blank')->render('iframe.index', $data);
    }




}