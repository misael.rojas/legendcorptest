<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?php echo asset('css/libs/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/home/iframe-widget.css') ?>" rel="stylesheet">
    <link href="<?php echo asset('css/home/jquery-widget.css') ?>" rel="stylesheet">
    <script src="<?php echo asset('js/libs/jquery.min.js') ?>"></script>
    <script src="<?php echo asset('js/jquery-plugin/tweet.refresher.js') ?>"></script>
    <style>
        .tweetContainer {
            max-height: none
        }
    </style>
</head>
<body>
<div id="tweetLoader" style="width: 100%; height: 100%">

</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#tweetLoader").refreshTweets({
            handle: "codinghorror"
        });
    });
</script>


</body>
</html>



