###LegendCorp Technical Test Brief
**Hi there!**

Congratulations on progressing to the next stage of our interview process, and thank you for agreeing to take the time to complete our technical test.

Please use the local setup instructions below. 

**Local setup**

1. The test requires PHP 7.1 or higher and a MySQL server.
2. Configure your webserver document root to be `/WebsiteCode/Public`. In Apache you might add this line to your config:
`DocumentRoot /var/www/html/LegendCorp/WebsiteCode/public`.
3. The home page has the instructions for the test.

If anything goes wrong or you have any questions please don't hesitate to get in touch.

Best of luck!
